//
//  AnalyzeData.h
//  WebMailC
//
//  Created by Gilbert Gwizdała on 06/12/2017.
//  Copyright © 2017 Gilbert Gwizdała. All rights reserved.
//

#ifndef AnalyzeData_h
#define AnalyzeData_h

#include <stdio.h>
#include <regex.h>
#include <string.h>
#include "Stack.h"

void analyze(char* data, char* fileName);

#endif /* AnalyzeData_h */
