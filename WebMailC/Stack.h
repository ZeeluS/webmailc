//
//  List.h
//  WebMailC
//
//  Created by Gilbert Gwizdała on 07/12/2017.
//  Copyright © 2017 Gilbert Gwizdała. All rights reserved.
//

#ifndef List_h
#define List_h

#include <stdio.h>
#include <stdlib.h>
#include "Stack.h"
#include <string.h>

struct Stack {
    char* text;
    struct Stack* next;
};

void pushStack(struct Stack** stack, char* text);
void freeStack(struct Stack** stack);
void pushIfNewStack(struct Stack** stack, char* text);

#endif /* List_h */
