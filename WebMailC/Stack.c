//
//  List.c
//  WebMailC
//
//  Created by Gilbert Gwizdała on 07/12/2017.
//  Copyright © 2017 Gilbert Gwizdała. All rights reserved.
//

#include "Stack.h"


void pushStack(struct Stack** stack, char* text) {
    struct Stack* lastElement = *stack;
    struct Stack* newElement = (struct Stack*) malloc(sizeof(struct Stack)) ;
    newElement->next = lastElement;
    newElement->text = text;
    *stack = newElement;
}

void freeStack(struct Stack** stack) {
    struct Stack* p = *stack;
    while (p != NULL) {
        struct Stack* next = p->next;
        free(p->text);
        free(p);
        p = next;
    }
}

void pushIfNewStack(struct Stack** stack, char* text) {

    struct Stack* p = *stack;
    while (p != NULL) {
        struct Stack* next = p->next;
        if (strcmp(text, p->text) == 0) {
            return;
        }
        p = next;
    }
    
    pushStack(stack, text);
}

