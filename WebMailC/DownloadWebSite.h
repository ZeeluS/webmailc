//
//  DownloadWebSite.h
//  WebMailC
//
//  Created by Gilbert Gwizdała on 06/12/2017.
//  Copyright © 2017 Gilbert Gwizdała. All rights reserved.
//

#ifndef DownloadWebSite_h
#define DownloadWebSite_h

#include <stdio.h>
#include <stdlib.h>
#include <curl/curl.h>

char* download_data(char*);

#endif /* DownloadWebSite_h */
