//
//  AnalyzeData.c
//  WebMailC
//
//  Created by Gilbert Gwizdała on 06/12/2017.
//  Copyright © 2017 Gilbert Gwizdała. All rights reserved.
//

#include "AnalyzeData.h"
#define MAXMATCH 10

void analyze(char* data, char* fileName) {
    regex_t regex;
    int reti;
    regmatch_t matches[MAXMATCH];
    
    struct Stack* stack;
    
    for (int i = 0; i < strlen(data); i++) {
        if( data[i] == '>' || data[i] == '<'){
            data[i] = '\n';
        }
    }
    
    /* Compile regular expression */
    reti = regcomp(&regex, "[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}", REG_EXTENDED);
    if (reti) {
        fprintf(stderr, "Could not compile regex\n");
        return;
    }
    
    char * curLine = data;
    while(curLine)
    {
        char * nextLine = strchr(curLine, '\n');
        if (nextLine) *nextLine = '\0';  // temporarily terminate the current line

        unsigned long len = strlen(curLine);
        //printf("%s\n", curLine);
        if(len >= 4) {
            reti = regexec(&regex, curLine, MAXMATCH, matches, 0);
            if (!reti) {
                for (int i=0; i < MAXMATCH; i++) {
                    if ((int)matches[i].rm_so < 0) break;
                    int numchars = (int)matches[i].rm_eo - (int)matches[i].rm_so;
                    char* email = (char*) calloc(100, sizeof(char));
                    strncpy(email, curLine+matches[i].rm_so, numchars);
                    email[numchars] = '\0';
                    printf("From %d to %d (%s)\n",(int)matches[i].rm_so, (int)matches[i].rm_eo, email);
                    pushIfNewStack(&stack, email);
                }
            }
        }

        if (nextLine) *nextLine = '\n';  // then restore newline-char, just to be tidy
        curLine = nextLine ? (nextLine+1) : NULL;
    }
    
    saveStackToFile(&stack, fileName);
    
    freeStack(&stack);
    regfree(&regex);
}


int saveStackToFile(struct Stack** stack, char* fileName) {
    FILE *fp;
    fp = fopen(fileName, "a");
    if (fp == NULL) {
        fprintf(stderr, "Error with open file");
        return -1;
    }
    
    struct Stack* p = *stack;
    while (p != NULL) {
        struct Stack* next = p->next;
        fprintf(fp, "%s\n", p->text);
        
        p = next;
    }
    
    return 0;
}


