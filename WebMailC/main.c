//
//  main.c
//  WebMailC
//
//  Created by Gilbert Gwizdała on 04.12.2017.
//  Copyright © 2017 Gilbert Gwizdała. All rights reserved.
//

#include <stdio.h>
#include "DownloadWebSite.h"
#include "AnalyzeData.h"

int main(int argc, const char * argv[]) {

    char * data = download_data("http://adwokatgb.pl/kontakt/");
    analyze(data, "a.txt");
    return 0;
}
